import { useTheMostClientContext } from "@/context/ApplicationContext";
import { gradeScalesMock } from "@/queries/mocks";
import { GradeScale } from "@/types/gradeScales";
import { useEffect, useState } from "react";

export default function useGetAllGradeScales() {
  const theMostClient = useTheMostClientContext();

  const [gradeScales, setGradeScales] = useState<GradeScale[]>([]);

  useEffect(() => {
    let ignore = false;

    if (process.env.NEXT_PUBLIC_MOCKED === "true") {
      setGradeScales(gradeScalesMock);
    } else {
      theMostClient
        .model("GradeScales")
        .getItems()
        .then((res) => {
          if (!ignore) {
            setGradeScales(res);
          }
        });
    }

    return () => {
      ignore = true;
    };
  }, [theMostClient]);

  return { gradeScales };
}
