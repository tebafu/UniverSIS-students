import { useTheMostClientContext } from "@/context/ApplicationContext";
import { recentGradesMock } from "@/queries/mocks";
import { any } from "@themost/query";
import { useEffect, useState } from "react";

function useGetRecentGrades() {
  const theMostClient = useTheMostClientContext();

  const [recentGrades, setRecentGrades] = useState<any[]>([]);
  const [examYear, setExamYear] = useState("");
  const [examPeriod, setExamPeriod] = useState("");

  useEffect(() => {
    if (process.env.NEXT_PUBLIC_MOCKED === "true") {
      setExamYear(recentGradesMock.gradeYear);
      setExamPeriod(recentGradesMock.examPeriod);
      setRecentGrades(recentGradesMock.recentGrades);
    } else {
      theMostClient
        .model("Students/Me/Grades")
        .select((x: any) => {
          return {
            examYear: x.courseExam.year,
            examYearName: x.courseExam.year.name,
            examPeriod: x.courseExam.examPeriod,
            examPeriodName: x.courseExam.examPeriod.name,
          };
        })
        .getItem()
        .then(({ examYear, examPeriod, examYearName, examPeriodName }) => {
          if (examYear) {
            setExamYear(examYearName);
            setExamPeriod(examPeriodName);
            theMostClient
              .model("Students/Me/Grades")
              .where(
                (x: any) => {
                  return x.courseExam.year == examYear && x.courseExam.examPeriod == examPeriod;
                },
                {
                  examYear,
                  examPeriod,
                }
              )
              .expand(
                any((x: any) => {
                  x.courseClass;
                }).expand(
                  any((y: any) => {
                    y.instructors;
                  }).expand(
                    any((z: any) => {
                      z.instructor;
                    }).select((x: any) => {
                      x.instructorSummary;
                    })
                  )
                ),
                (x: any) => {
                  x.course;
                }
              )
              .getItems()
              .then((result) => {
                setRecentGrades(result);
              });
          }
        });
    }
  }, [theMostClient]);

  return { recentGrades, examYear, examPeriod };
}

export default useGetRecentGrades;
