import { useTheMostClientContext } from "@/context/ApplicationContext";
import { allCoursesMock } from "@/queries/mocks";
import { any } from "@themost/query";
import { useEffect, useState } from "react";

function useGetAllCourses() {
  const theMostClient = useTheMostClientContext();

  const [allCourses, setAllCourses] = useState<any[]>([]);

  useEffect(() => {
    if (process.env.NEXT_PUBLIC_MOCKED === "true") {
      setAllCourses(allCoursesMock);
    } else {
      theMostClient
        .model("Students/Me/Courses")
        .asQueryable()
        .expand(
          any((x: any) => {
            x.course;
          }).expand(
            any((z: any) => {
              z.instructor;
            }).select((x: any) => {
              x.instructorSummary;
            })
          )
        )
        .getItems()
        .then((courses) => {
          setAllCourses(courses);
        });
    }
  }, [theMostClient]);

  return allCourses;
}

export default useGetAllCourses;
