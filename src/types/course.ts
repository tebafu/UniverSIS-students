// TODO: change to actual types later
export interface FullCourse {
  id: string;
  courseExam: number;
  student: number;
  courseClass: CourseClass;
  grade1: number;
  status: number;
  grade2?: null;
  gradeModified: string;
  examGrade: number;
  percentileRank?: null;
  dateModified: string;
  formattedGrade: string;
  course: Course;
  isPassed: number;
}
export interface CourseClass {
  id: string;
  course: string;
  year: number;
  period: number;
  status: number;
  statusModified: string;
  maxNumberOfStudents?: null;
  minNumberOfStudents?: null;
  weekHours: number;
  totalHours?: null;
  absenceLimit?: null;
  numberOfStudents: number;
  title: string;
  mustRegisterSection: boolean;
  department: string;
  dateModified: string;
  instructors?: InstructorsEntity[] | null;
}
export interface InstructorsEntity {
  id: string;
  courseClass: string;
  instructor: Instructor;
  allowEdit?: null;
  teachingHours?: null;
  dateModified: string;
}
export interface Instructor {
  id: number;
  familyName: string;
  givenName: string;
  category: string;
  department: string;
  locale?: null;
}
export interface Course {
  id: string;
  displayCode: string;
  department: string;
  courseArea?: null | number;
  name: string;
  subtitle?: null;
  isEnabled: boolean;
  isShared: boolean;
  gradeScale: number;
  instructor: number;
  isCalculatedInScholarship: boolean;
  units: number;
  courseUrl?: null;
  notes?: null;
  replacedByCourse?: null;
  replacedCourse?: null;
  maxNumberOfRemarking: number;
  parentCourse?: null;
  coursePartPercent?: null;
  calculatedCoursePart: boolean;
  courseStructureType: number;
  courseSector?: null;
  courseCategory: number | null;
  ects: number;
  isLocal: boolean;
  dateModified: string;
  calculatedInRegistration: boolean;
  locale: Locale;
}

export interface Locale {
  id: string;
  object: string;
  inLanguage: string;
  name: string;
}

interface GradeYear {
  id: number;
  identifier: null;
  additionalType: string;
  alternateName: string;
  description: string;
  image: null;
  name: string;
  url: null;
  dateCreated: null;
  dateModified: null;
  createdBy: null;
  modifiedBy: null;
}
