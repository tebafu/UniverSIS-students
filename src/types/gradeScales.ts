export interface GradeValue {
  id: number;
  name: string | null;
  alternateName: string;
  gradeScale: number;
  valueFrom: number;
  valueTo: number;
  exactValue: number;
}

export interface GradeScale {
  id: number;
  name: string;
  scaleType: number;
  scaleFactor: number;
  scaleBase: number;
  formatPrecision: number;
  scalePrecision: number;
  step: number | null;
  locale: string | null;
  values: GradeValue[];
}
