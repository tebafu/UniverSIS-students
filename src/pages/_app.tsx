import AuthGuard from "@/components/common/AuthGuard";
import { ApplicationContextProvider } from "@/context/ApplicationContext";
import { AuthProvider } from "@/context/AuthContext";
import "@/styles/globals.css";
import theme from "@/styles/theme";
import { ChakraProvider } from "@chakra-ui/react";
import type { AppProps } from "next/app";
import { useEffect, useState } from "react";

export default function App({ Component, pageProps }: AppProps) {
  const [isClient, setIsClient] = useState(false);

  useEffect(() => {
    setIsClient(true);
  }, []);

  if (!isClient) return;

  if (process.env.NEXT_PUBLIC_MOCKED === "true") {
    return (
      <ChakraProvider theme={theme}>
        <Component {...pageProps} />
      </ChakraProvider>
    );
  }

  return (
    <ChakraProvider theme={theme}>
      <ApplicationContextProvider>
        <AuthProvider>
          <AuthGuard>
            <Component {...pageProps} />
          </AuthGuard>
        </AuthProvider>
      </ApplicationContextProvider>
    </ChakraProvider>
  );
}
