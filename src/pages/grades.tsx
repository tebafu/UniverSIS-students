import DefaultLayout from "@/components/layouts/Default";
import AllGrades from "@/components/pages/grades/AllGrades";
import GradeFiltersMenu from "@/components/pages/grades/GradesFilterMenu";
import HeaderCards from "@/components/pages/grades/HeaderCards";
import useGetAllCourses from "@/queries/useGetAllCourses";
import { reduceCoursesByType } from "@/utilities/reduceCoursesByType";
import { useState } from "react";

export interface Filters {
  typeOfGrouping: "semester" | "courseType";
  statusOfCourses: "all" | "incomplete" | "passed" | "thesis";
  search: string;
}

export default function Grades() {
  const allCourses = useGetAllCourses();
  const [filters, setFilters] = useState<Filters>({ typeOfGrouping: "semester", statusOfCourses: "all", search: "" });

  const groups = reduceCoursesByType(allCourses, filters.typeOfGrouping);

  const filteredCourses = allCourses.filter((course: any) => {
    const matchesCode = course.course.displayCode.includes(filters.search);
    const matchesName = course.courseTitle.toLowerCase().includes(filters.search.toLowerCase());
    let status = false;
    if (filters.statusOfCourses === "all") {
      status = true;
    } else if (filters.statusOfCourses === "incomplete") {
      status = !course.isPassed;
    } else if (filters.statusOfCourses === "passed") {
      status = course.isPassed;
    }
    return (matchesCode || matchesName) && status;
  });

  return (
    <DefaultLayout>
      <div className="grid gap-4 md:gap-6">
        <HeaderCards />
        <GradeFiltersMenu filters={filters} setFilters={setFilters} />
        <AllGrades groups={groups} courses={filteredCourses} groupingType={filters.typeOfGrouping} />
      </div>
    </DefaultLayout>
  );
}
