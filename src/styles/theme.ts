import { extendTheme } from "@chakra-ui/react";

const theme = extendTheme({
  styles: {
    global: {
      body: {
        color: "text",
      },
    },
  },
  colors: {
    background: "#1A202C",
    text: "#FFFFFFCC",
    border: "#2D3748",
  },
  components: {
    Heading: {
      baseStyle: {
        color: "#FFFFFFCC",
      },
    },
    Text: {
      baseStyle: {
        color: "#FFFFFFCC",
      },
      variants: {
        cardSubtitle: {
          color: "gray",
          fontSize: "xs",
        },
      },
    },
    Modal: {
      baseStyle: {
        dialog: {
          bg: "background",
          color: "text",
          border: "1px solid",
        },
      },
    },
    Radio: {
      defaultProps: {
        colorScheme: "green",
      },
    },
    Input: {
      variants: {
        outline: {
          field: {
            color: "text",
          },
        },
      },
    },
  },
});

export default theme;
