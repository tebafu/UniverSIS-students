export function calculateStatsFromCourseGroup(courses: any) {
  const passed = courses.filter((course: any) => course.isPassed).length;
  const total = courses.length;
  const totalGraded = courses.filter((course: any) => course.grade !== null).length;
  const { passedECTS, totalECTS, gradeSum } = courses.reduce(
    (acc: any, course: any) => {
      acc.totalECTS += course.course.ects;
      acc.gradeSum += course.grade;
      if (course.isPassed) {
        acc.passedECTS += course.course.ects;
      }
      return acc;
    },
    { passedECTS: 0, totalECTS: 0, gradeSum: 0 }
  );
  const average = (gradeSum / totalGraded) * 10;
  return {
    passed,
    total,
    passedECTS,
    totalECTS,
    average: isNaN(average) ? 0 : average,
  };
}
