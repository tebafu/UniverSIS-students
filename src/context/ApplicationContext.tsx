import configuration from "@/config/app.json";
import { ReactDataContext } from "@themost/react";
import { createContext, useContext, useState } from "react";

interface ApplicationContextType {
  theMostClient: ReactDataContext;
  configuration: typeof configuration;
}

const theMostClient = new ReactDataContext(configuration.settings.remote.server);

const ApplicationContext = createContext<ApplicationContextType>({
  theMostClient: theMostClient,
  configuration: configuration,
});

export const ApplicationContextProvider = ({ children }: { children: React.ReactNode }) => {
  const [theMostClient, _] = useState(new ReactDataContext(configuration.settings.remote.server));

  return <ApplicationContext.Provider value={{ theMostClient, configuration }}>{children}</ApplicationContext.Provider>;
};

export const useApplicationContext = () => {
  const context = useContext(ApplicationContext);
  if (context === undefined) {
    throw new Error("useApplicationContext must be used within an ApplicationContextProvider");
  }
  return context;
};

export const useConfigContext = () => {
  const context = useContext(ApplicationContext);
  if (context === undefined) {
    throw new Error("useConfigContext must be used within an ApplicationContextProvider");
  }
  return context.configuration;
};

export const useTheMostClientContext = () => {
  const context = useContext(ApplicationContext);
  if (context === undefined) {
    throw new Error("useTheMostClientContext must be used within an ApplicationContextProvider");
  }
  return context.theMostClient;
};
