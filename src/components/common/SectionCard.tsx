import { Card, CardBody, CardHeader, Divider } from "@chakra-ui/react";
import React from "react";

const SectionCard = ({
  header,
  children,
  noPadding = false,
}: {
  header?: React.ReactNode;
  children: React.ReactNode;
  noPadding?: boolean;
}) => {
  return (
    <Card
      bg={"#1A202C"}
      bgGradient="radial(rgba(26, 32, 44, 0.1), rgba(190, 227, 248, 0.1))"
      border="1px solid #FFFFFFCC"
    >
      {header && (
        <>
          <CardHeader>{header}</CardHeader>
          <Divider borderColor={"#718096"} />
        </>
      )}
      <CardBody p={noPadding ? 0 : undefined}>{children}</CardBody>
    </Card>
  );
};

export default SectionCard;
