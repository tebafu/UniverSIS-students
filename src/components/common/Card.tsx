import React from "react";

const Card = ({ children }: { children: React.ReactNode }) => {
  return <div className="h-32 bg-backgroundDark rounded-xl">{children}</div>;
};

export default Card;
