import { Card, CardBody, CardHeader, Divider, Heading, Text } from "@chakra-ui/react";
import React from "react";

const PanelCard = ({ title, period, children }: { title: string; period: string; children: React.ReactNode }) => {
  return (
    <Card
      bg={"#1A202C"}
      bgGradient="radial(rgba(26, 32, 44, 0.1), rgba(190, 227, 248, 0.1))"
      border="1px solid #FFFFFFCC"
    >
      <CardHeader>
        <div className="flex">
          <Heading flexShrink={0} size="md">
            {title} •&nbsp;
          </Heading>
          <Text fontSize="md">{period}</Text>
        </div>
      </CardHeader>
      <Divider borderColor={"#718096"} />
      <CardBody>{children}</CardBody>
    </Card>
  );
};

export default PanelCard;
