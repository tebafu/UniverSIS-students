import { Divider, Heading, Text } from "@chakra-ui/react";
import { motion } from "framer-motion";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import Button from "./Button";

const NavItem = ({ icon, text, url }: { icon: string; text: string; url: string }) => {
  const router = useRouter();
  const path = router.pathname;

  const isActive = path === url;

  console.log(isActive, path, url);
  return (
    <Link href={url}>
      <div className={`h-full py-2 px-1 ${isActive && "bg-white/20"} md:p-4 md:flex md:gap-2`}>
        <div className="flex items-center justify-center mb-1 md:mb-0">
          <Image src={`/icons/${icon}${!isActive ? "-dark" : ""}.svg`} alt="dashboard" width={32} height={32} />
        </div>
        <Heading size="xs" alignContent="center" textAlign="center">
          {text}
        </Heading>
      </div>
    </Link>
  );
};

const SidebarItem = ({ icon, text }: { icon: string; text: string }) => {
  return (
    <div className="flex gap-4">
      <Image src={icon} alt={text} width={24} height={24} />
      <Text>{text}</Text>
    </div>
  );
};

const SideNavItem = ({ icon, text, link }: { icon: string; text: string; link: string }) => {
  return (
    <Link href={link}>
      <SidebarItem icon={icon} text={text} />
    </Link>
  );
};

const SideNav = () => {
  return (
    <div className="bg-[#1A202C] px-4 py-8 col-span-3 h-screen">
      <div className="grid gap-4">
        <div className="flex flex-col items-center gap-1">
          <Image src="/img/avatar.png" alt="avatar" width={74} height={74} />
          <Text>Stefanos Giannakos</Text>
        </div>
        <Divider />
        <Heading size="sm" pl="10">
          Student
        </Heading>
        <SideNavItem icon="icons/person.svg" text="About Me" link="/about" />
        <SideNavItem icon="icons/graduation.svg" text="Graduation" link="/graduation" />
        <Divider />
        <Heading size="sm" pl="10">
          Support
        </Heading>
        <SideNavItem icon="icons/requests.svg" text="Requests" link="/requests" />
        <SideNavItem icon="icons/help.svg" text="Help" link="/help" />
        <SideNavItem icon="icons/benefits.svg" text="Benefits" link="/benefits" />
        <Divider />
        <Heading size="sm" pl="10">
          Preferances
        </Heading>
        <SidebarItem icon="icons/moon.svg" text="Theme" />
        <SidebarItem icon="icons/language.svg" text="Language" />
        <div className="flex justify-center">
          <Button
            text="Log Out"
            icon="/icons/left-exit.svg"
            handleClick={() => {
              console.log("logout redirect");
            }}
          />
        </div>
      </div>
    </div>
  );
};

const DesktopSideNav = () => {
  return (
    <motion.div
      whileHover={{ width: 256 }}
      className="md:absolute z-40 h-screen w-14 overflow-hidden border-whitecc border-r hidden md:block"
    >
      <div className="w-64">
        <SideNav />
      </div>
    </motion.div>
  );
};

const MobileSideNav = ({ handleClose }: { handleClose: () => void }) => {
  return (
    <div className="fixed left-0 h-screen w-full z-50 md:hidden grid grid-cols-5">
      <div onClick={handleClose} className="col-span-2"></div>
      <motion.div initial={{ x: "100%" }} animate={{ x: 0 }} className="rounded-l-3xl col-span-3 overflow-hidden">
        <SideNav />
      </motion.div>
    </div>
  );
};

const MainNavbar = ({
  setMobileSideNavOpen,
}: {
  setMobileSideNavOpen: React.Dispatch<React.SetStateAction<boolean>>;
}) => {
  const toggleSideNav = () => {
    setMobileSideNavOpen((prev) => !prev);
  };

  return (
    <div className="border-b border-whitecc bg-[#171923] flex justify-between">
      <div className="flex gap-4 p-4 z-50">
        <Image src="/icons/logo-universis.svg" alt="Universis" width={38} height={38} />
        <Heading size="lg" alignContent="center">
          Dashboard
        </Heading>
      </div>
      <div className="grid grid-cols-4 fixed bottom-0 z-50 w-full bg-[#171923] border-t border-whitecc rounded-t-md md:static md:flex md:border-none md:w-fit">
        <NavItem icon="dashboard" text="Dashboard" url="/" />
        <NavItem icon="nav-star" text="Grades" url="/grades" />
        <NavItem icon="grid" text="Registrations" url="/registrations" />
        <NavItem icon="schedule" text="Schedule" url="/schedule" />
      </div>
      <div className="flex md:hidden p-4 gap-4">
        <div className="m-auto">
          <Image src="/icons/bell.svg" alt="notifications" width={32} height={32} />
        </div>
        <div className="m-auto" onClick={toggleSideNav}>
          <Image src="/icons/hamburger.svg" alt="sidebar-toggle" width={32} height={32} />
        </div>
      </div>
    </div>
  );
};

export default function Navbar() {
  const [mobileSideNavOpen, setMobileSideNavOpen] = useState(false);
  return (
    <div>
      <MainNavbar setMobileSideNavOpen={setMobileSideNavOpen} />
      {mobileSideNavOpen && <MobileSideNav handleClose={() => setMobileSideNavOpen(false)} />}
      <DesktopSideNav />
    </div>
  );
}
