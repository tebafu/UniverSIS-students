import { Button as ChakraButton, Text } from "@chakra-ui/react";
import Image from "next/image";

function Button({
  text,
  icon,
  handleClick,
  chakraProps,
}: {
  icon?: string;
  text: string;
  handleClick: () => void;
  chakraProps?: any;
}) {
  return (
    <ChakraButton variant="outline" _hover={{}} {...chakraProps} onClick={handleClick}>
      <div className="flex gap-2">
        {icon && <Image src={icon} alt={text} width={14} height={14} />}
        <Text>{text}</Text>
      </div>
    </ChakraButton>
  );
}

export default Button;
