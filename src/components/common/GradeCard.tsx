import useGetGradeStatistics from "@/queries/useGetGradeStatistics";
import { Box, Flex, Heading, IconButton, Text } from "@chakra-ui/react";
import Image from "next/image";
import { useState } from "react";

interface GradeCardProps {
  title: string;
  code: string;
  teachers?: string[];
  grade: string;
  isPassed?: boolean;
  courseExamId: string | null;
  drawer?: React.ReactNode;
}

// todo: decide on a graph library or implement a custom one
const GradeCardStatistics = ({ courseExamId }: { courseExamId: string }) => {
  const { average, averagePass, graded, passing, brackets } = useGetGradeStatistics(courseExamId);
  return (
    <div>
      <div className="grid grid-cols-5">
        {brackets.map((bracket) => (
          <Text key={bracket.to}>
            {bracket.to}-{bracket.total}
          </Text>
        ))}
      </div>
      <div className="grid grid-cols-2">
        <Text variant={"cardSubtitle"}>Graded: {graded}</Text>
        <Text variant={"cardSubtitle"}>Passing: {passing}</Text>
        <Text variant={"cardSubtitle"}>Average: {average}</Text>
        <Text variant={"cardSubtitle"}>Average Pass: {averagePass}</Text>
      </div>
    </div>
  );
};

const GradeCard = ({ title, code, teachers, grade, isPassed = false, courseExamId, drawer }: GradeCardProps) => {
  const [flipped, setFlipped] = useState(false);
  const [drawerOpen, setDrawerOpen] = useState(false);

  const toggleDrawer = () => {
    setDrawerOpen((prev) => !prev);
  };

  const flipCard = () => {
    setFlipped((prev) => !prev);
  };

  return (
    <div className="bg-backgroundDark rounded-xl h-fit">
      <div className="h-32 flex gap-2 pr-2">
        <Flex
          flexGrow={1}
          justifyContent="space-between"
          borderRight="1px solid #718096"
          alignItems={!flipped ? "center" : "flex-end"}
          p={4}
        >
          {!flipped ? (
            <>
              <Box>
                <Heading fontSize="sm">{title.toLocaleUpperCase()}</Heading>
                <Text variant="cardSubtitle">
                  {code} {teachers && " • " + teachers.join(", ")}
                </Text>
              </Box>
              <Text fontSize="3xl" color={isPassed ? "green.400" : "gray.400"}>
                {grade}
              </Text>{" "}
            </>
          ) : (
            courseExamId && <GradeCardStatistics courseExamId={courseExamId} />
          )}
        </Flex>
        <IconButton
          aria-label="statistics"
          height="100%"
          backgroundColor="transparent"
          _hover={{ backgroundColor: "transparent" }}
          _active={{ backgroundColor: "transparent" }}
          _focus={{ backgroundColor: "transparent" }}
          onClick={flipCard}
          icon={
            <Image
              alt={flipped ? "statistics" : "grade"}
              src={flipped ? "/icons/star.svg" : "/icons/graph.svg"}
              width={20}
              height={20}
            />
          }
        />
      </div>
      {drawer && (
        <div className="border-t border-[#718096]">
          <div className="flex justify-center">
            <IconButton
              aria-label="more info"
              backgroundColor="transparent"
              _active={{ backgroundColor: "transparent" }}
              _focus={{ backgroundColor: "transparent" }}
              onClick={toggleDrawer}
              icon={
                <Image
                  alt={drawerOpen ? "close" : "open"}
                  src={`/icons/arrow-${drawerOpen ? "up" : "down"}.svg`}
                  width={24}
                  height={19}
                />
              }
            />
          </div>
          {drawerOpen && <div className="p-4">{drawer}</div>}
        </div>
      )}
    </div>
  );
};

export default GradeCard;
