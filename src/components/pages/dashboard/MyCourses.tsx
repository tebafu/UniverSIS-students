import CourseCard from "@/components/common/CourseCard";
import PanelCard from "@/components/common/PanelCard";
import useGetRecentCourses from "@/queries/useGetRecentCourses";

const mockedCourse = {
  title: "learning theories & educational software",
  code: "NCO-5-5",
  teacher: "Apostolos Papadopoulos",
};

const MyCourses = () => {
  const { currentRegistration } = useGetRecentCourses();

  return (
    <PanelCard
      title="My Courses"
      period={currentRegistration?.registrationPeriod?.name + " " + currentRegistration?.registrationYear?.name}
    >
      <div className="grid grid-cols-1 gap-2 md:grid-cols-2">
        {currentRegistration.classes?.map((course: any) => (
          <CourseCard
            key={course.id}
            code={course.courseClass.course.displayCode}
            teachers={
              course.courseClass.instructors?.map(
                (instructor: any) => instructor.instructor.givenName + " " + instructor.instructor.familyName
              ) ?? []
            }
            title={course.courseClass.title}
          />
        ))}
      </div>
    </PanelCard>
  );
};

export default MyCourses;
