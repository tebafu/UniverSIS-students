import PanelCard from "@/components/common/PanelCard";
import PeriodProgress from "@/components/common/PeriodProgress";
import useGetRecentGrades from "@/queries/useGetRecentGrades";
import { Flex, Text } from "@chakra-ui/react";
import GradeCard from "../../common/GradeCard";

const RecentGrades = () => {
  const { recentGrades, examYear, examPeriod } = useGetRecentGrades();

  const passedCourses = recentGrades.filter((course) => course.isPassed).length;

  return (
    <PanelCard title="Recent Grades" period={examPeriod + " " + examYear}>
      <Flex align={"center"} gap={2} mb={2.5}>
        <PeriodProgress passed={passedCourses} total={recentGrades.length} />
        <Text>
          {passedCourses}/{recentGrades.length}
        </Text>
      </Flex>
      <div className="grid grid-cols-1 gap-2 md:grid-cols-2">
        {recentGrades.map((gradeRecord) => (
          <GradeCard
            key={gradeRecord.id}
            title={gradeRecord.course.name}
            teachers={
              gradeRecord.courseClass.instructors?.map(
                (instructor: any) => instructor.instructor.givenName + " " + instructor.instructor.familyName
              ) ?? []
            }
            grade={gradeRecord.formattedGrade}
            isPassed={gradeRecord.isPassed}
            code={gradeRecord.course.displayCode}
            courseExamId={gradeRecord.courseExam.toString()}
          />
        ))}
      </div>
    </PanelCard>
  );
};

export default RecentGrades;
