import { Text } from "@chakra-ui/react";
import Image from "next/image";

function Roadmap() {
  return (
    <div className="p-2 md:p-4">
      <div className="flex mb-2 gap-2">
        <div className="flex items-center flex-shrink-0">
          <Image src="/img/avatar.png" alt="aristotle" width={45} height={45} />
        </div>
        <div>
          <Text mb={-2}>Hello,</Text>
          <Text>Stefanos GIannakos</Text>
        </div>
        <div className="flex-grow">
          <div className="bg-background py-2 px-4 rounded-lg border border-border w-fit ml-auto">
            <Text>Average: 8.1</Text>
          </div>
        </div>
      </div>
      <div className="flex">
        <div className="bg-background py-1 px-2 rounded-lg border border-border">
          <Text>Semester</Text>
        </div>
        <div className="w-full relative flex justify-between items-center px-2">
          <div className="h-2 w-11/12 absolute left-0 bg-gradient-to-r from-[#39475e] via-green-400 via-20% to-[#39475E]"></div>
          <div className="relative size-4 bg-green-400 rounded-full"></div>
          <div className="relative size-6 bg-green-400 rounded-full">
            <Text textAlign={"center"}>7</Text>
          </div>
          <div className="relative size-4 bg-[#39475E] rounded-full"></div>
          <Image className="relative" src="/icons/graduation-gray.svg" alt="upwards trend" width={24} height={24} />
        </div>
      </div>
    </div>
  );
}

export default Roadmap;
