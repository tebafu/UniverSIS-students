import { Heading, Text } from "@chakra-ui/react";
import Image from "next/image";

function Notifications() {
  return (
    <div className="p-2 md:p-4">
      <div className="flex gap-4 justify-center items-center mb-2">
        <div className="flex items-center flex-shrink-0">
          <Image src="/img/aristotle.png" alt="aristotle" width={34} height={34} />
        </div>
        <Heading size={{ base: "sm", md: "lg" }} fontWeight={"base"}>
          Aristotle University of Thessaloniki
        </Heading>
      </div>
      <div className="flex justify-between bg-gradient-to-r from-red-700 via-background via-10% to-background py-3 px-2 rounded-lg border border-border">
        <div className="flex gap-2">
          <Text fontWeight={"bold"}>3</Text>
          <Image src="/icons/megaphone.svg" alt="megaphone" width={24} height={24} />
        </div>
        <Text>Latest: September exams of 23...</Text>
        <Image src="/icons/arrow-right.svg" alt="arrow right" width={24} height={24} />
      </div>
    </div>
  );
}

export default Notifications;
