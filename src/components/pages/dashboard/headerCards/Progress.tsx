import useGetAllCourses from "@/queries/useGetAllCourses";
import { calculateStatsFromCourseGroup } from "@/utilities/calculateStatsFromCourseGroup";
import { CircularProgress, CircularProgressLabel, Text } from "@chakra-ui/react";
import Image from "next/image";

function Progress() {
  // ! make all courses call only once and here get it from context
  const allCourses = useGetAllCourses();
  const { passed, total, passedECTS, totalECTS, average } = calculateStatsFromCourseGroup(allCourses);

  return (
    <div className="p-1">
      <div className="bg-background m-1 p-2 md:p-4 rounded-lg border border-border flex justify-between items-center">
        <div className="flex items-center flex-shrink-0">
          <Image src="/icons/rise.svg" alt="upwards trend" width={24} height={24} />
        </div>
        <Text>Progress</Text>
        <CircularProgress
          value={average ? (average / 10) * 100 : 0}
          color="green.400"
          size="80px"
          trackColor={"#1A202C"}
        >
          <CircularProgressLabel>
            <Text fontSize={"xs"} mb={-1}>
              {average.toFixed(2).replace(/[.,]00$/, "")}/10
            </Text>
            <Text variant={"cardSubtitle"}>Average</Text>
          </CircularProgressLabel>
        </CircularProgress>
        <CircularProgress
          value={total ? (passed / total) * 100 : 0}
          color="green.400"
          size="80px"
          trackColor={"#1A202C"}
        >
          <CircularProgressLabel>
            <Text fontSize={"xs"} mb={-1}>
              {passed}/{total}
            </Text>
            <Text variant={"cardSubtitle"}>Passed</Text>
          </CircularProgressLabel>
        </CircularProgress>
        <CircularProgress
          value={totalECTS ? (passedECTS / totalECTS) * 100 : 0}
          color="green.400"
          size="80px"
          trackColor={"#1A202C"}
        >
          <CircularProgressLabel>
            <Text fontSize={"xs"} mb={-1}>
              {passedECTS}/{totalECTS}
            </Text>
            <Text variant={"cardSubtitle"}>ECTS</Text>
          </CircularProgressLabel>
        </CircularProgress>
      </div>
    </div>
  );
}

export default Progress;
