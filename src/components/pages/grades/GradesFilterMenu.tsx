import SectionCard from "@/components/common/SectionCard";
import { Filters } from "@/pages/grades";
import { Input, InputGroup, InputRightElement } from "@chakra-ui/react";
import Image from "next/image";
import GradesFiltersModal from "./GradesFilterModal";

function GradeFilters({
  filters,
  setFilters,
}: {
  filters: Filters;
  setFilters: React.Dispatch<React.SetStateAction<Filters>>;
}) {
  return (
    <div>
      <SectionCard noPadding>
        <div className="flex p-2 gap-4">
          <InputGroup>
            <Input
              value={filters.search}
              onChange={(e) => setFilters({ ...filters, search: e.target.value })}
              placeholder="Course or Code..."
            />
            <InputRightElement>
              {filters.search ? (
                <Image
                  src="/icons/x.svg"
                  onClick={() => setFilters({ ...filters, search: "" })}
                  width={20}
                  height={20}
                  alt="search"
                />
              ) : (
                <Image src="/icons/search.svg" width={20} height={20} alt="search" />
              )}
            </InputRightElement>
          </InputGroup>
          <GradesFiltersModal filters={filters} setFilters={setFilters} />
        </div>
      </SectionCard>
    </div>
  );
}

export default GradeFilters;
