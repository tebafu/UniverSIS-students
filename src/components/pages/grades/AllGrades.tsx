import GradeCard from "@/components/common/GradeCard";
import PeriodProgress from "@/components/common/PeriodProgress";
import SectionCard from "@/components/common/SectionCard";
import { calculateStatsFromCourseGroup } from "@/utilities/calculateStatsFromCourseGroup";
import { Heading, Text } from "@chakra-ui/react";
import PeriodSatistics from "./PeriodSatistics";

export default function AllGrades({
  groups,
  groupingType,
  courses,
}: {
  groups: any;
  groupingType: "semester" | "courseType";
  courses: any;
}) {
  const groupsWithCourses = groups.map((group: any) => {
    return {
      ...group,
      coursesOfGroup: courses.filter((course: any) => course[groupingType].id === group.id),
    };
  });

  return (
    <div className="flex flex-col gap-8">
      {groupsWithCourses.map((group: any) => {
        const { passed, total, passedECTS, totalECTS, average } = calculateStatsFromCourseGroup(group.coursesOfGroup);

        const periodSatistics = [
          { label: "passed", value: `${passed}/${total}` },
          { label: "average", value: `${average.toFixed(2)}` },
          { label: "ECTS", value: `${passedECTS}/${totalECTS}` },
        ];

        return (
          group.coursesOfGroup.length > 0 && (
            <SectionCard key={group.id} header={<Heading textAlign="center">{group.name}</Heading>}>
              <div className="flex flex-col gap-4 mb-2">
                <PeriodProgress passed={passed} total={total} />
                <div className="grid grid-cols-1 gap-2 md:grid-cols-2">
                  {group.coursesOfGroup.map((course: any) => {
                    console.log(course);
                    const drawerContent = (
                      <div>
                        <div className="flex justify-between">
                          <Text>
                            TYPE: <span className="text-gray-500">{course.courseType.abbreviation}</span>
                          </Text>
                          <Text>
                            ECTS : <span className="text-gray-500">{course.course.ects}</span>
                          </Text>
                          <Text>
                            EXAM PERIOD :{" "}
                            <span className="text-gray-500">
                              {course.gradePeriodDescription + course.gradeYear?.id}
                            </span>
                          </Text>
                        </div>
                        <Text>
                          INSTRUCTOR :{" "}
                          <span className="text-gray-500">
                            {course.course.instructor?.givenName + " " + course.course.instructor?.familyName}
                          </span>
                        </Text>
                      </div>
                    );

                    return (
                      <GradeCard
                        key={course.id}
                        title={course.courseTitle}
                        grade={course.formattedGrade}
                        isPassed={course.isPassed}
                        code={course.course.displayCode}
                        courseExamId={course.gradeExam}
                        drawer={drawerContent}
                      />
                    );
                  })}
                </div>
                <PeriodSatistics stats={periodSatistics} />
              </div>
            </SectionCard>
          )
        );
      })}
    </div>
  );
}
