import { Text } from "@chakra-ui/react";

function PeriodSatistics({ stats }: { stats: { label: string; value: string }[] }) {
  return (
    <div className=" bg-backgroundDark rounded-xl flex justify-around p-2">
      {stats.map((stat) => {
        return (
          <div key={stat.label} className="flex gap-2">
            <Text color="green.400">{stat.value}</Text>
            <Text>{stat.label}</Text>
          </div>
        );
      })}
    </div>
  );
}

export default PeriodSatistics;
